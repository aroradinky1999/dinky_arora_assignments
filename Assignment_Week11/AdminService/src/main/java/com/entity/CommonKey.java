package com.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@SuppressWarnings("serial")
@Embeddable
@ToString
@NoArgsConstructor
@RequiredArgsConstructor
public class CommonKey implements Serializable {
 
	private int id;
	private String name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	

}
