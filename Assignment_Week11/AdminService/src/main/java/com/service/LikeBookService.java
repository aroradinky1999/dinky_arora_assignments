package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.LikeBooksDao;
import com.entity.LikeBooks;


@Service
public class LikeBookService {
	@Autowired
	LikeBooksDao likeBookDao;
	
	
	
	
	public List<LikeBooks> viewLikeBooks(String name) {
		
		return likeBookDao.viewLikeBooks(name);

	}

	
	

}
