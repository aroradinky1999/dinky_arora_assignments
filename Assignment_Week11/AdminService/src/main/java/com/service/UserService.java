package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.UserDao;
import com.entity.User;

@Service
public class UserService {

	@Autowired
	UserDao userDao;
	
	public List<User> getAllUser(){
		return userDao.findAll();
	}
	
	public String storeUserInfo(User user) {
		if(userDao.existsById(user.getEmail())) {
			return "User id must be unique";
		}else {
			userDao.save(user);
			return "User stored successfully";
		}
	}
	
	public String deleteUserInfo(String email) {
		if(!userDao.existsById(email)) {
			return "User id not available";
		}else {
			userDao.deleteById(email);
			return "User deleted";
		}
	}
	
	public String updateUserInfo(User user) {
		if(!userDao.existsById(user.getEmail())) {
			return "User id not available";
		}else {
			User u = userDao.getById(user.getEmail());
			u.setName(user.getName());
			u.setEmail(user.getEmail());
			u.setPassword(user.getPassword());
			userDao.saveAndFlush(u);		
			return "User updated";
		}
	}


}
