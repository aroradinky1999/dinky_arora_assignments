package com.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.web.client.RestTemplate;

import com.entity.Admin;

@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
class AdminControllerTest {
	String baseUrl = "http://localhost:8585/admin";

	@Test
	void testAdminLogin() {
		// fail("Not yet implemented");
		RestTemplate restTemplate = new RestTemplate();
		Admin admin = new Admin();
		admin.setEmail("dinkyarora456@gmail.com");
		admin.setPassword("1111");

		String result = restTemplate.postForObject(baseUrl + "/adminLogin", admin, String.class);
		Assertions.assertEquals(result,"Sucessfully logged in");

	}

	@Test
	void testAdminRegister() {
		// fail("Not yet implemented");
		RestTemplate restTemplate = new RestTemplate();
		Admin admin = new Admin();
		admin.setEmail("dinkyarora456@gmail.com");
		admin.setPassword("1111");

		String result = restTemplate.postForObject(baseUrl + "/adminRegister", admin, String.class);
		Assertions.assertEquals(" Already registered", result);

	}

	@Test
	void testLogout() {
		// fail("Not yet implemented");
		RestTemplate restTemplate = new RestTemplate();
		String result = restTemplate.getForObject(baseUrl + "/adminLogOut/dinkyarora456@gmail.com", String.class);
		Assertions.assertEquals(" Successfully Log Out", result);

	}

}
