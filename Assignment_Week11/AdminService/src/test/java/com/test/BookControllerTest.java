package com.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.entity.Books;
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
class BookControllerTest {
	String baseUrl = "http://localhost:8686/books";

	@Test
	void testGetAllBookssInfo() {
		//fail("Not yet implemented");
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<List<Books>> books = restTemplate.exchange(baseUrl+"/getAllBooks",HttpMethod.GET,null,new ParameterizedTypeReference<List<Books>>() {});
		List<Books>lb=books.getBody();
		Assertions.assertTrue(lb.stream().count()==8);

	}

	@Test
	void testStoreBooksInfo() {
		//fail("Not yet implemented");
		

		
	}

	@Test
	void testDeleteBooksInfo() {
		//fail("Not yet implemented");
	}

	@Test
	void testUpdateBooksInfo() {
		//fail("Not yet implemented");
	}

}
