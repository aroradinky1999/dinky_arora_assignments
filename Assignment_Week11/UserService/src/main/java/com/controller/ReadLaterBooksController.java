package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.ReadLaterBooks;
import com.service.ReadLaterBooksService;

@RestController
@RequestMapping("/readLaterBook")
public class ReadLaterBooksController {
	@Autowired
	ReadLaterBooksService readLaterBooksService;
	
	@PostMapping(value = "addToReadLaterBook", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String addToReadLaterBook(@RequestBody ReadLaterBooks readLaterBookss) {
		return readLaterBooksService.addToReadLaterBooks(readLaterBookss);
	}
	
	@GetMapping(value = "viewReadLaterBook/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ReadLaterBooks> getReadLaterdBook(@PathVariable("name") String name) {
		return readLaterBooksService.viewReadLaterBooks(name);
	}


}
