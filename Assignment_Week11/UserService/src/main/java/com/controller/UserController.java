package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.User;
import com.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	UserService userService;
	
	@PostMapping(value = "userLogin", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String userLogin( @RequestBody User user) {
		return userService.login(user);
	}

	@PostMapping(value = "userRegister", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String userRegister( User user) {
		return userService.registerUserInfo(user);
	}
	
	@GetMapping(value = "userLogOut/{email}")
	public String logout(@PathVariable("email") String email) {
		return userService.logOut(email);
	}
	
	



}
