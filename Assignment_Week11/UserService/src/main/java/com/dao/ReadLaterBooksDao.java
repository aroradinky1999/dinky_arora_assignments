package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entity.CommonKey;
import com.entity.ReadLaterBooks;

@Repository
public interface ReadLaterBooksDao extends JpaRepository<ReadLaterBooks,CommonKey> {
 
	@Query("select u from ReadLaterBooks u where u.commonKey.name=:name")
	public List<ReadLaterBooks> viewReadLaterBooks(@Param("name") String name);
}
