package com.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Entity
@ToString
@NoArgsConstructor
@RequiredArgsConstructor
public class ReadLaterBooks {
	
	private String title;
	private String author;
	private float price;
	 @EmbeddedId
	private CommonKey commonKey;
	
	
	
	public CommonKey getCommonKey() {
		return commonKey;
	}
	public void setCommonKey(CommonKey commonKey) {
		this.commonKey = commonKey;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	
	
	

}
