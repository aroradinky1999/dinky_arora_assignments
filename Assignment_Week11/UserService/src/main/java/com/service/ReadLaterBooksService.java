package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.ReadLaterBooksDao;
import com.entity.ReadLaterBooks;

@Service
public class ReadLaterBooksService {
	@Autowired
	ReadLaterBooksDao readLaterBooksDao;
	
	
	public String addToReadLaterBooks(ReadLaterBooks readLaterBooks) {
		if(readLaterBooksDao.existsById(readLaterBooks.getCommonKey())) {
			return "Already present in readLater book";
		}else {
			readLaterBooksDao.save(readLaterBooks);
			return "Book Stored Sucessfully";
		}
		
	}
	
	public List<ReadLaterBooks> viewReadLaterBooks(String name) {
		
		return readLaterBooksDao.viewReadLaterBooks(name);

	}

}
