package com.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.UserDao;

import com.entity.User;

@Service
public class UserService {

	@Autowired
	UserDao userDao;
	
	
	

	public String registerUserInfo(User user) {
		if (userDao.existsById(user.getEmail())) {
			return "Failed to store user details";
		} else {
		userDao.save(user);
		return "Welcome! , sucessfully registered ";
		}

	}

	public String login(User user) {
		if(!userDao.existsById(user.getEmail())) {
			return "Either email or password is incorrect";
		}
		else {
		
			User a = userDao.getById(user.getEmail());
			
				if (a.getPassword().equals(user.getPassword())) {
					return "Welcome!, sucessfully logged in ";
				} else {
					return "Sorry!"+ user.getEmail() + " ,Failed to login";
				}
		}
	}
	

	public String logOut(String email) {
		if (userDao.existsById(email)) {
			return " Successfully Log Out";
		} else {
			return email + " Failed to Log Out";
		}

	}
}
