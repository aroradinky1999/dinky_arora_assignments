package com.service.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.entity.CommonKey;
import com.entity.LikeBooks;
import com.service.LikeBookService;
@SpringBootTest
class LikeBookServiceTest {
   @Autowired
   LikeBookService likeBookService;
   
	@Test
	void testAddToLikeBooks() {
		//fail("Not yet implemented");
		LikeBooks likeBooks = new LikeBooks();
		likeBooks.setCommonKey(new CommonKey(1, "sonali"));
		likeBooks.setTitle("A Brief History of Time");
		likeBooks.setAuthor(" Stephen W.Hawking");
		likeBooks.setPrice(1100);
		String res =likeBookService.addToLikeBooks(likeBooks);
		Assertions.assertEquals(res, "Already present in like book");
	}

	@Test
	void testViewLikeBooks() {
		//fail("Not yet implemented");
		List<LikeBooks> lb = likeBookService.viewLikeBooks("sonali");
		Assertions.assertTrue(lb.stream().allMatch(b->b.getTitle().equals("A Brief History of Time")));
		
	}

}
