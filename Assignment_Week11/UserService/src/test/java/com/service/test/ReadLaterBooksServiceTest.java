package com.service.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.entity.CommonKey;
import com.entity.LikeBooks;
import com.entity.ReadLaterBooks;
import com.service.ReadLaterBooksService;
@SpringBootTest
class ReadLaterBooksServiceTest {
	@Autowired
	ReadLaterBooksService readLaterBooksService;
	
	@Test
	void testAddToReadLaterBooks() {
		//fail("Not yet implemented");
		ReadLaterBooks readLaterBooks = new ReadLaterBooks();
		readLaterBooks.setCommonKey(new CommonKey(6, "sonali"));
		readLaterBooks.setTitle(" What If?");
		readLaterBooks.setAuthor(" Randall Munrof");
		readLaterBooks.setPrice(3040);
		String res = readLaterBooksService.addToReadLaterBooks(readLaterBooks);
		Assertions.assertEquals("Already present in readLater book", res);
		
	}

	@Test
	void testViewReadLaterBooks() {
		//fail("Not yet implemented");
		List<ReadLaterBooks> lb = readLaterBooksService.viewReadLaterBooks("sonali");
		Assertions.assertTrue(lb.stream().allMatch(b -> b.getAuthor().equals(" Randall Munrof")));
	}

}
