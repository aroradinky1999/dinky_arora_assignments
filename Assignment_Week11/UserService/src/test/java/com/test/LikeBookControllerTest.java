package com.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.entity.CommonKey;
import com.entity.LikeBooks;

@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
class LikeBookControllerTest {
	String baseUrl = "http://localhost:8686/likeBook";

	@Test
	void testAddToLikeBook() {
		// fail("Not yet implemented");
		RestTemplate restTemplate = new RestTemplate();

		LikeBooks likeBooks = new LikeBooks();
		likeBooks.setCommonKey(new CommonKey(1, "sonali"));
		likeBooks.setTitle("A Brief History of Time");
		likeBooks.setAuthor(" Stephen W.Hawking");
		likeBooks.setPrice(1100);
		

		String res = restTemplate.postForObject(baseUrl + "/addToLikeBook", likeBooks, String.class);

		
		Assertions.assertEquals("Already present in like book", res);

	}

	@Test
	void testGetLikedBook() {
		// fail("Not yet implemented");
		RestTemplate restTemplate = new RestTemplate();
		
		ResponseEntity<List<LikeBooks>> book=  restTemplate.exchange(baseUrl+"/viewLikeBook/sonali",HttpMethod.GET,null,new ParameterizedTypeReference<List<LikeBooks>>() {});
		List<LikeBooks> lb= book.getBody();

		Assertions.assertTrue(lb.stream().allMatch(b->b.getTitle().equals("A Brief History of Time")));

	}

}
