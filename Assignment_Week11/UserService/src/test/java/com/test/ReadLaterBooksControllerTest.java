package com.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.entity.CommonKey;

import com.entity.ReadLaterBooks;

@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
class ReadLaterBooksControllerTest {
	String baseUrl = "http://localhost:8686/readLaterBook";

	@Test
	void testAddToReadLaterBook() {
		// fail("Not yet implemented");
		RestTemplate restTemplate = new RestTemplate();

		ReadLaterBooks readLaterBooks = new ReadLaterBooks();
		readLaterBooks.setCommonKey(new CommonKey(6, "sonali"));
		readLaterBooks.setTitle(" What If?");
		readLaterBooks.setAuthor(" Randall Munrof");
		readLaterBooks.setPrice(3040);

		String res = restTemplate.postForObject(baseUrl + "/addToReadLaterBook", readLaterBooks, String.class);

		Assertions.assertEquals("Already present in readLater book", res);

	}

	@Test
	void testGetReadLaterdBook() {
		// fail("Not yet implemented");

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<List<ReadLaterBooks>> book = restTemplate.exchange(baseUrl + "/viewReadLaterBook/sonali", HttpMethod.GET,
				null, new ParameterizedTypeReference<List<ReadLaterBooks>>() {
				});
		List<ReadLaterBooks> lb = book.getBody();

		Assertions.assertTrue(lb.stream().allMatch(b -> b.getAuthor().equals(" Randall Munrof")));

	}

}
