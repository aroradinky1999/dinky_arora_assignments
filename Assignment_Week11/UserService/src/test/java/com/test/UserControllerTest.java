package com.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.web.client.RestTemplate;

import com.entity.User;

@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
class UserControllerTest {
	String baseUrl = "http://localhost:8686/user";

	@Test
	void testUserLogin() {
		// fail("Not yet implemented");
		RestTemplate restTemplate = new RestTemplate();

		User user = new User();
		user.setEmail("shristy123@gmail.com");
		user.setPassword("1111");

		String result = restTemplate.postForObject(baseUrl + "/userLogin", user, String.class);
		Assertions.assertEquals(result,"Welcome!, sucessfully logged in ");

	}

	

	@Test
	void testLogout() {
		// fail("Not yet implemented");
		RestTemplate restTemplate = new RestTemplate();		
		String result = restTemplate.getForObject(baseUrl+"/userLogOut/shristy123@gmail.com", String.class);		
		Assertions.assertEquals(result," Successfully Log Out");

	}

}
