import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {
  loginRef = new FormGroup({
    email:new FormControl("", Validators.required),
    password:new FormControl("", Validators.required)
  });
  RegisterRef = new FormGroup({
   email:new FormControl("", Validators.required),
   name:new FormControl("",Validators.required),
   password:new FormControl("", Validators.required)
 });
 loginRes:string="";
registerRes:string="";
  constructor(public user:UserService, public route:Router) { }

  ngOnInit(): void {
  }

  checkUser():void{
    let login = this.loginRef.value;
    
    this.user.userLoginDetails(login).subscribe(res=>{
      if(res.includes('sucessfully logged in')){
        sessionStorage.setItem("uname",login.email);
        this.route.navigate(['userHome']);
   }else{
    this.loginRes = 'Failed to login!!Please enter correct email and password'
   }
    },error=>console.log(error));
  }

  registerUser():void{
    let register = this.RegisterRef.value;
    this.registerRes="";
    this.user.userRegisterDetails(register).subscribe(res=>this.registerRes=res,error=>console.log(error));
    this.RegisterRef.reset();
  }

}
