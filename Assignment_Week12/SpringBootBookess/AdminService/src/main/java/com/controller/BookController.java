package com.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.entity.Books;
//import com.service.BookService;
import com.entity.User;

@RestController
@RequestMapping("/book")
@CrossOrigin
public class BookController {
	
	@Autowired
	RestTemplate restTemplate;

	
	@GetMapping(value="getallbooks")
	public List<Object> getAllBooks(){
		String Url="http://user-client:8686/books/getAllBooks";
		Object[] res = restTemplate.getForObject(Url, Object[].class);
		return Arrays.asList(res);
	}
	
	@PostMapping(value = "storebook",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeBooks(@RequestBody Object obj) {
		String Url="http://user-client:8686/books/storeBooks/";
		ResponseEntity<String> user= restTemplate.postForEntity(Url, obj, String.class);
		return user.getBody();
	}

  



	@PutMapping(value = "updateBook", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String updateBooks(@RequestBody Books books ) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<Books> entity = new HttpEntity<>(books,headers);
		String url = "http://user-client:8686/books/updateBooks";
		return restTemplate.exchange(url, HttpMethod.PUT, entity, String.class).getBody();
		
	}
	
	
	@DeleteMapping(value = "deleteUserInfo/{id}")
	public String deleteUserInfo(@PathVariable("id") int id)
	{
		  HttpHeaders headers = new HttpHeaders();
	      headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	      HttpEntity<Books> entity = new HttpEntity<>(headers);
		String url="http://user-client:8686/books/deleteBooks/"+id;
		return restTemplate.exchange(url , HttpMethod.DELETE, entity, String.class).getBody();
		
	}



}


