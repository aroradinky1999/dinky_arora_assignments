create database  dinky_week11;

use  dinky_week11;

create table Admin
 (
 email varchar(40) primary key,
 password varchar(40)
 );

 create table User
 (
 
 name varchar(40),
 email varchar(40) primary key,
 password varchar(40)
 );

 
 select * from user;
  
 create table books
 (
 id int primary key,
 title varchar(200),
 author varchar(40),
 price float
 );
 
 insert into books values(1, 'A Brief History of Time' , 'Stephen W.Hawking',1100.00);
 insert into books values(2,'The Science Book','Dan Green',1724.00);
 insert into books values(3,'The Sleeping Beauties','Suzanne O Sullivan',1505.00);
 insert into books values(4,'Breath: The New Science of a Lost Art','James Nestor',1405.00);
 insert into books values(5,'Surely youre Joking Mr Feynman','Richard P Feynman',3004.00);
 insert into books values(6,'What If?','Randall Munrof',3040.00);
 insert into books values(7,'Invisible Women','Caroline Criado Perez',1090.00);
 insert into books values(8,'Under a White Sky: The Nature of the Future','Elizabeth Kolbert',2340.00);
 
  select * from books;
  
  create table like_books
 (
 lbid int primary key auto_increment,
 id int,
 title varchar(200),
 author varchar(40),
 price float,
 name varchar(40)
 );
 
 create table read_later_books
 (
 rlbid int primary key auto_increment,
 id int,
 title varchar(200),
 author varchar(40),
 price float,
 name varchar(40)
 );
  
  
  
 
 