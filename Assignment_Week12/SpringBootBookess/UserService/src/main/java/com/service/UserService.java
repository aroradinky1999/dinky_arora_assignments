package com.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.UserDao;

import com.entity.User;

@Service
public class UserService {

	@Autowired
	UserDao userDao;
	
	public List<User> getAllUserDetails() {
		return userDao.findAll();
	}

	public String storeUserInfo(User user) {
		if (userDao.existsById(user.getEmail())) {
			return user.getEmail() +" ,Failed to store user details";
		} else {
		userDao.save(user);
		return "Welcome! "+user.getEmail() +" ,sucessfully registered ";
		}

	}

	public String login(User user) {
		if(!userDao.existsById(user.getEmail())) {
			return "Either email or password is incorrect";
		}
		else {
		
			User a = userDao.getById(user.getEmail());
			
				if (a.getPassword().equals(user.getPassword())) {
					return "Welcome!, "+  user.getEmail() + " ,sucessfully logged in ";
				} else {
					return "Sorry!"+ user.getEmail() + " ,Failed to login";
				}
		}
	}
	

	public String logOut(String email) {
		if (userDao.existsById(email)) {
			return email +" Successfully Log Out";
		} else {
			return email + " Failed to Log Out";
		}

	}
	
	public String deleteUserInfo(String email) {
		if (!userDao.existsById(email)) {
			return "User  " + email + " details not present";
		} else {
			userDao.deleteById(email);
			return "User " + email + " deleted successfully";
		}
	}

	
	public String updateUser(User user) {
		if (!userDao.existsById(user.getEmail())) {
			return "User " + user.getEmail() + " details not present";
		} else {
			User u = userDao.getById(user.getEmail());
			u.setName(user.getName());
			u.setPassword(user.getPassword());
			userDao.saveAndFlush(u);
			return "User " + user.getEmail() + " details updated successfully";
		}

	}
	
	
	
	

}
