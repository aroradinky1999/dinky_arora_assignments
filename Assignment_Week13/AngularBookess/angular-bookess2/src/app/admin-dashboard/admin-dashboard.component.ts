import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Books } from '../books';
import { BooksService } from '../books.service';
import { User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
name:string="";
addRes:string="";
delRes:string="";
books:Array<Books>=[];
flag:boolean=false;
updateRes:string="";
addUserRes:string="";
delUserRes:string="";
updateUserRes:string="";
users:Array<User>=[];

AddRef = new FormGroup({
  id:new FormControl("", Validators.required),
  title:new FormControl("", Validators.required),
  author:new FormControl("", Validators.required),
  price:new FormControl("", Validators.required)
});
UpdateRef = new FormGroup({
  id:new FormControl("", Validators.required),
  title:new FormControl("", Validators.required),
  author:new FormControl("", Validators.required),
  price:new FormControl("", Validators.required)
});
AddUserRef = new FormGroup({
  email:new FormControl("", Validators.required),
  name:new FormControl("",Validators.required),
  password:new FormControl("", Validators.required)
});
UpdateUserRef = new FormGroup({
  email:new FormControl("", Validators.required),
  name:new FormControl("",Validators.required),
  password:new FormControl("", Validators.required)
});
  constructor(public router:Router,public book:BooksService,public user:UserService) {
   
   }

  ngOnInit(): void {
    this.loadProducts();
    this.loadUsers();

    let obj = sessionStorage.getItem("uname");
    if(obj != null ){
      this.name =obj;
    }
  }

  logout(){
 this.router.navigate(["adminLogin"]);
  }
  storeBook():void{
    let addbook = this.AddRef.value; 
    this.book.addBooksDetails(addbook).subscribe(res=>this.addRes=res,error=>console.log(error),()=>this.loadProducts());
    this.AddRef.reset();
  }
  loadProducts():void{
    this.book.loadBookDetails().subscribe(res=>this.books=res)
  }
  deleteBook(id:number){
    this.book.deleteBooksDetails(id).subscribe(res=>this.delRes=res,error=>console.log(error),()=>this.loadProducts());
  }
  updateBook(bookess:Books){
  this.flag=true;
  
  }

  updateBookInfo():void{
    let updatebook = this.UpdateRef.value;
  this.book.updateBooksDetails(updatebook).subscribe(res=>this.updateRes=res,error=>console.log(error),()=>this.loadProducts());
  this.UpdateRef.reset();
  }
  // crud for user

  storeUser():void{
    let adduser = this.AddUserRef.value; 
    this.user.addUserDetails(adduser).subscribe(res=>this.addUserRes=res,error=>console.log(error),()=>this.loadUsers());
    this.AddUserRef.reset();
  }
  loadUsers():void{
    this.user.loadUserDetails().subscribe(res=>this.users=res)
  }
  deleteUser(email:string){
    this.user.deleteUserDetails(email).subscribe(res=>this.delUserRes=res,error=>console.log(error),()=>this.loadUsers());
  }
  updateUser(userss:User){
  this.flag=true; 
  }

  updateUserInfo():void{
    let updateuser = this.UpdateUserRef.value;
  this.user.updateUserDetails(updateuser).subscribe(res=>this.updateUserRes=res,error=>console.log(error),()=>this.loadUsers());
  this.UpdateRef.reset();
  }
}
