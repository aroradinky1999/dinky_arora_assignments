import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { Books } from './books';
@Injectable({
  providedIn: 'root'
})
export class BooksService {

  constructor(public http:HttpClient) { }

  loadBookDetails():Observable<Books[]>{
  return this.http.get<Books[]>("http://localhost:8686/books/getAllBooks")
 
  }
  addBooksDetails(book:Books):Observable<string>{
    return this.http.post("http://localhost:8585/book/storebook",book,{responseType:'text'})
    }

  deleteBooksDetails(id:number):Observable<string>{
      return this.http.delete("http://localhost:8585/book/deleteUserInfo/"+id,{responseType:'text'})
    }

  updateBooksDetails(book:Books):Observable<string>{
        return this.http.put("http://localhost:8585/book/updateBook",book,{responseType:'text'})
    }

}
