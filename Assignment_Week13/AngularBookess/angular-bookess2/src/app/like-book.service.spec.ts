import { TestBed } from '@angular/core/testing';

import { LikeBookService } from './like-book.service';

describe('LikeBookService', () => {
  let service: LikeBookService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LikeBookService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
