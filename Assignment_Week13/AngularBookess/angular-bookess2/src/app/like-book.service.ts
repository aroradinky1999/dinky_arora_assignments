import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { LikeBooks } from './like-books';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class LikeBookService {

  constructor(public http:HttpClient) { }

  loadLikeBookDetails(email:string):Observable<LikeBooks[]>{
    return this.http.get<LikeBooks[]>("http://localhost:8686/likeBook/viewLikeBook/"+email)
   
    }
    addLikeBooksDetails(likebook:LikeBooks):Observable<string>{
      return this.http.post("http://localhost:8686/likeBook/addToLikeBook",likebook,{responseType:'text'})
      }
}
