import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Books } from '../books';
import { BooksService } from '../books.service';
import { LikeBookService } from '../like-book.service';
import { LikeBooks } from '../like-books';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit {
name:string='';
books:Array<Books>=[];
likeBooks:Array<LikeBooks>=[];
  constructor(public router:Router,public book:BooksService,public likebook:LikeBookService) { 
    
  }

  ngOnInit(): void {
    let obj = sessionStorage.getItem("uname");
    if(obj != null ){
      this.name =obj;
    }
  }
  logout(){
    this.router.navigate(["userLogin"]);
     }

     loadProducts():void{
      this.book.loadBookDetails().subscribe(res=>this.books=res)
    }
    view(email : string):void{
      this.likebook.loadLikeBookDetails(email).subscribe(res=>this.likeBooks=res)
    }

}
