import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http';
import { User } from './user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor( public http:HttpClient) { }

  userLoginDetails(user:User):Observable<string>{
    return this.http.post("http://localhost:8686/user/userLogin",user,{responseType:'text'})

  }

  userRegisterDetails(user:User):Observable<string>{
    return this.http.post("http://localhost:8686/user/userRegister",user,{responseType:'text'})
    }

    loadUserDetails():Observable<User[]>{
      return this.http.get<User[]>("http://localhost:8585/users/getallusers")
     
      }
      addUserDetails(user:User):Observable<string>{
        return this.http.post("http://localhost:8585/users/storeUser",user,{responseType:'text'})
        }
    
      deleteUserDetails(email:string):Observable<string>{
          return this.http.delete("http://localhost:8585/users/deleteUserInfo/"+email,{responseType:'text'})
        }
    
      updateUserDetails(user:User):Observable<string>{
            return this.http.put("http://localhost:8585/users/updateUserInfo",user,{responseType:'text'})
        }
  

}
