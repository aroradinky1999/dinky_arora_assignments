import { Component, OnInit } from '@angular/core';
import { Books } from '../books';
import { BooksService } from '../books.service';

@Component({
  selector: 'app-view-books',
  templateUrl: './view-books.component.html',
  styleUrls: ['./view-books.component.css']
})
export class ViewBooksComponent implements OnInit {
  books:Array<Books>=[];
  constructor(public book:BooksService) { }

  ngOnInit(): void {
  }
  loadProducts():void{
    this.book.loadBookDetails().subscribe(res=>this.books=res)
  }
}
