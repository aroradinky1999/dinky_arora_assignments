package com.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Admin;
import com.service.AdminService;

@RestController
@RequestMapping("/admin")
@CrossOrigin
public class AdminController {
	
	@Autowired
	AdminService adminService;
	
	@PostMapping(value = "adminLogin", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String adminLogin(@RequestBody Admin admin) {
		return adminService.login(admin);
	}

	@PostMapping(value = "adminRegister", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String adminRegister(@RequestBody Admin admin) {
		return adminService.registerAdminInfo(admin);
	}
	
	@GetMapping(value = "adminLogOut/{email}")
	public String logout(@PathVariable("email") String email) {
		return adminService.logOut(email);
	}

	

}
