package com.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;




@RestController
@RequestMapping("/likeBooks")
@CrossOrigin
public class LikeBookController {
	
	@Autowired
	RestTemplate restTemplate;

	
	@GetMapping(value="getlikebooks/{email}")
	public List<Object> getAllBooks(@PathVariable("email") String email){
		String Url="http://user-client:8686/likeBook/viewLikeBook/"+email;
		Object[] res = restTemplate.getForObject(Url, Object[].class);
		return Arrays.asList(res);
	}
	
	
	

}
