package com.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.entity.User;



@RestController
@RequestMapping("/users")
@CrossOrigin
public class UserController {
	
	
	
	@Autowired
	RestTemplate restTemplate;

	
	@GetMapping(value="getallusers")
	public List<Object> getAllUser(){
		String Url="http://user-client:8686/user/getuser";
		Object[] res = restTemplate.getForObject(Url, Object[].class);
		return Arrays.asList(res);
	}
	
	@PostMapping(value = "storeUser",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeUser(@RequestBody Object obj) {
		String Url="http://user-client:8686/user/storeUser/";
		ResponseEntity<String> user= restTemplate.postForEntity(Url, obj, String.class);
		return user.getBody();
	}

  



	@PutMapping(value = "updateUserInfo", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String updateUserPassword(@RequestBody User user ) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<User> entity = new HttpEntity<>(user,headers);
		String url = "http://user-client:8686/user/updateUser";
		return restTemplate.exchange(url, HttpMethod.PUT, entity, String.class).getBody();
		
	}
	
	
	
	@DeleteMapping(value = "deleteUserInfo/{email}")
	public String deleteUserInfo(@PathVariable("email") String  email)
	{
		  HttpHeaders headers = new HttpHeaders();
	      headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	      HttpEntity<User> entity = new HttpEntity<>(headers);
		String url="http://user-client:8686/user/deleteUser/"+email;
		return restTemplate.exchange(url , HttpMethod.DELETE, entity, String.class).getBody();
		
	}






}
