package com.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.AdminDao;
import com.entity.Admin;

@Service
public class AdminService {

	@Autowired
	AdminDao adminDao;

	public String registerAdminInfo(Admin admin) { 
		if (adminDao.existsById(admin.getEmail())) {
			return " Already registered";
		} else {
			adminDao.save(admin);
			return "Welcome admin!"+ admin.getEmail() + " , sucessfully registered ";
		}
	}
	
	
	public String login(Admin admin) {
		if(!adminDao.existsById(admin.getEmail())) {
			return "Either email or password is incorrect";
		}
		else {
			Admin a = adminDao.getById(admin.getEmail());
			if (a.getPassword().equals(admin.getPassword())) {
				return admin.getEmail() +" Sucessfully logged in";
			}
			else {
				return admin.getEmail() +" Failed to login";
			}
		}
	}

	public String logOut(String email ) {
		if(adminDao.existsById(email)) {
			return " Successfully Log Out";
		}
		else {
			return email +"Failed to Log Out";
		}
		
	}

	
	
	

}


	
