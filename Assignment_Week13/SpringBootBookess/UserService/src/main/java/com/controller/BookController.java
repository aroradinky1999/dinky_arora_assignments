package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Books;
import com.service.BookService;

@RestController
@RequestMapping("/books")
@CrossOrigin
public class BookController {
	
	@Autowired
	BookService bookService;

	@GetMapping(value = "getAllBooks", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Books> getAllBookssInfo() {
		return bookService.getAllBooks();
	}

	@PostMapping(value = "storeBooks", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeBooksInfo(@RequestBody Books books) {
		return bookService.storeBooksInfo(books);
	}

	@DeleteMapping(value = "deleteBooks/{bId}")
	public String deleteBooksInfo(@PathVariable("bId") int bId) {
		return bookService.deleteBooksInfo(bId);
	}
	
	@PutMapping(value = "updateBooks")
	public String updateBooksInfo(@RequestBody Books books) {
		return bookService.updateBooksInfo(books);
	}
	
}


