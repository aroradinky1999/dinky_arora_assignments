package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.LikeBooks;
import com.service.LikeBookService;

@RestController
@RequestMapping("/likeBook")
@CrossOrigin
public class LikeBookController {
	
	@Autowired
	LikeBookService likeBookService;
	
	@PostMapping(value = "addToLikeBook", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String addToLikeBook(@RequestBody LikeBooks likeBooks) {
		return likeBookService.addToLikeBooks(likeBooks);
	}
	
	@GetMapping(value = "viewLikeBook/{email}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<LikeBooks> getLikedBook(@PathVariable("email") String email) {
		return likeBookService.viewLikeBooks(email);
	}

	

}
