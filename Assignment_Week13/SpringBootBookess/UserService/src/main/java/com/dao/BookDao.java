package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Books;

@Repository
public interface BookDao extends JpaRepository<Books, Integer>{

}
