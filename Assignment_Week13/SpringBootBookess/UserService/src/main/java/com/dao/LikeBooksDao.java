package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entity.CommonKey;
import com.entity.LikeBooks;




@Repository
public interface LikeBooksDao extends JpaRepository<LikeBooks,CommonKey> {

	@Query("select u from LikeBooks u where u.commonKey.email=:email")
	public List<LikeBooks> viewLikeBooks(@Param("email") String email);

}
