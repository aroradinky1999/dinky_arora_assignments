package com.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@SuppressWarnings("serial")
@Embeddable
@ToString
@NoArgsConstructor
@RequiredArgsConstructor
public class CommonKey implements Serializable {
 
	private int id;
	private String email;
	
	
	public CommonKey() {
		super();
		
	}
	public CommonKey(int id, String email) {
		super();
		this.id = id;
		this.email = email;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
	

}
