package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.BookDao;
import com.entity.Books;

@Service
public class BookService {
	
	@Autowired
	BookDao bookDao;
	
	public List<Books> getAllBooks(){
		return bookDao.findAll();
	}
	
	public String storeBooksInfo(Books books) {
		if(bookDao.existsById(books.getId())) {
			return "Books id must be unique";
		}else {
			bookDao.save(books);
			return "Books stored successfully";
		}
	}
	
	public String deleteBooksInfo(int bId) {
		if(!bookDao.existsById(bId)) {
			return "Books id not available";
		}else {
			bookDao.deleteById(bId);
			return "Books deleted";
		}
	}
	
	public String updateBooksInfo(Books books) {
		if(!bookDao.existsById(books.getId())) {
			return "Books id not available";
		}else {
			Books b = bookDao.getById(books.getId());
			b.setPrice(books.getPrice());
			b.setAuthor(books.getAuthor());
			b.setTitle(books.getTitle());
			bookDao.saveAndFlush(b);		
			return "Books updated";
		}
	}


}
