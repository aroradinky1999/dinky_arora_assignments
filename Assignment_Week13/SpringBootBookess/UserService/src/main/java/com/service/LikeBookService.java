package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.LikeBooksDao;
import com.entity.LikeBooks;


@Service
public class LikeBookService {
	@Autowired
	LikeBooksDao likeBookDao;
	
	
	
	public String addToLikeBooks(LikeBooks likeBooks) {
		if(likeBookDao.existsById(likeBooks.getCommonKey())) {
			return "Already present in like book";
		}else {
			likeBookDao.save(likeBooks);
			return "Added to Liked Section";
		}
		
	}
	
	public List<LikeBooks> viewLikeBooks(String email) {
		
		return likeBookDao.viewLikeBooks(email);

	}

	
	

}
