
create database travel_dinky;

use travel_dinky;

create table PASSENGER
 (
  Passenger_name varchar(20), 
  Category     varchar(20),
   Gender      varchar(20),
   Boarding_City      varchar(20),
   Destination_City   varchar(20),
  Distance              int,
  Bus_Type     varchar(20)
);


create table PRICE
(
             Bus_Type   varchar(20),
             Distance    int,
              Price      int
          );

insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');

select * from passenger;

insert into price values('Sleeper',350,770);
insert into price values('Sleeper',500,1100);
insert into price values('Sleeper',600,1320);
insert into price values('Sleeper',700,1540);
insert into price values('Sleeper',1000,2200);
insert into price values('Sleeper',1200,2640);
insert into price values('Sleeper',350,434);
insert into price values('Sitting',500,620);
insert into price values('Sitting',500,620);
insert into price values('Sitting',600,744);
insert into price values('Sitting',700,868);
insert into price values('Sitting',1000,1240);
insert into price values('Sitting',1200,1488);
insert into price values('Sitting',1500,1860);

select * from price;
/*1*/
 select count(gender) as female_travelled_min600 from passenger where distance >= 600 and gender="f";
 select count(gender) as male_travelled_min600 from passenger where distance>=600 and gender="m";
 /*2*/
select min(price) as minPrice_Sleeper from price where bus_type = 'sleeper';
 /*3*/
select passenger_name from passenger where passenger_name like 'S%';
 /*4*/
select pass.passenger_name,pass.boarding_city,pass.destination_city,pass.Bus_type,p.price from passenger pass,price p where pass.distance=p.distance and pass.bus_type=p.bus_type;
  /*5*/
select pass.passenger_name,p.price  from passenger pass,price p where pass.distance=p.distance and pass.bus_type=p.bus_type and pass.distance=1000 and pass.bus_type='sitting';
  /*6*/ 
select p.bus_type as Charges_Pallavi ,p.price from price p,passenger pass where pass.passenger_name='pallavi' and pass.distance=p.distance ;
  /*7*/ 
select distinct distance as Dis_desc  from passenger  order by distance desc;
  /*8*/ 
select passenger_name,distance*100/(select sum(distance) from passenger) as Dis_percentage from passenger;
  /*9*/ 
create view pass_view as select passenger_name as passenger_name_travelled_in_AC from passenger where category='AC';
select * from pass_view;
 /*10*/  
delimiter &&
create procedure get_total_passenger_sleeper()
begin
select count(bus_type) as passenger_in_sleeperBus from passenger where bus_type='sleeper';
end &&
     
call get_total_passenger_sleeper() &&
 /*11*/    
select * from passenger limit 5;