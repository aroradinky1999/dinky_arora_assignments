package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bean.LikeBooks;

/**
 * Servlet implementation class LikeBookController
 */
@WebServlet("/LikeBookController")
public class LikeBookController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LikeBookController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		response.setContentType("text/html");

		try (PrintWriter out = response.getWriter()) {
			ArrayList<LikeBooks> likeBook = new ArrayList<>();

			int id = Integer.parseInt(request.getParameter("id"));
			LikeBooks lb = new LikeBooks();
			lb.setId(id);

			HttpSession session = request.getSession();
			ArrayList<LikeBooks> like_book = (ArrayList<LikeBooks>) session.getAttribute("like-book");

			if (like_book == null) {
				likeBook.add(lb);
				session.setAttribute("like-book", likeBook);
				response.sendRedirect("displaybooksafterlogin.jsp");
			} else {
				likeBook = like_book;
				boolean exist = false;
				for (LikeBooks l : like_book) {
					if (l.getId() == id) {
						exist = true;
						out.print("<h3 style='color:crimson; text-align:center'>Item already exist in Liked Page.<a href = likebooks.jsp>Go to Liked Books Page</a></h3>'");
					}
				}
				if (!exist) {
					likeBook.add(lb);
					response.sendRedirect("displaybooksafterlogin.jsp");
				}
			}

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
