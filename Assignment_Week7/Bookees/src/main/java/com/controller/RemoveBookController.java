package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bean.LikeBooks;

/**
 * Servlet implementation class RemoveBookController
 */
@WebServlet("/RemoveBookController")
public class RemoveBookController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveBookController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		response.setContentType("text/html");
		try (PrintWriter out = response.getWriter()){
			String id =request.getParameter("id");
			if(id !=null) {
				ArrayList<LikeBooks> like_book = (ArrayList<LikeBooks>) request.getSession().getAttribute("like-book");
				if(like_book != null) {
					for(LikeBooks lb : like_book) {
						if(lb.getId()==Integer.parseInt(id)) {
							like_book.remove(like_book.indexOf(lb));
							break;
						}
					}
					response.sendRedirect("likebooks.jsp");
				}
			}else {
				response.sendRedirect("likebooks.jsp");
			}
			
		} catch (Exception e) {
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
