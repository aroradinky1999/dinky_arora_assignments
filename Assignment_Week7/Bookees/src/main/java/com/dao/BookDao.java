package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.bean.Books;
import com.resource.BookeesConnection;

public class BookDao {

	public List<Books> getAllBooks(){
		List<Books> listOfbooks = new ArrayList<Books>();
		try {
			Connection con= BookeesConnection.getDbConnection();
			PreparedStatement pstmt= con.prepareStatement("select * from books");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				Books books = new Books();
				books.setImg(rs.getString(1));
				books.setId(rs.getInt(2));
				books.setTitle(rs.getString(3));
				books.setAuthor(rs.getString(4));
				books.setPrice(rs.getFloat(5));
				listOfbooks.add(books);
			}
		 }catch(Exception e) {
			System.out.println("Get all books Method Exception "+e);
		}
		
		return listOfbooks;
	  }

}
