package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.bean.LikeBooks;
import com.resource.BookeesConnection;

public class LikeBooksDao {

	public List<LikeBooks> getAllLikeBooks(ArrayList<LikeBooks> likeBook) {
		List<LikeBooks> listOfLikebooks = new ArrayList<LikeBooks>();
		try {
			if (likeBook.size() > 0) {
				for (LikeBooks item : likeBook) {
					Connection con = BookeesConnection.getDbConnection();
					PreparedStatement pstmt = con.prepareStatement("select * from books where id=?");
					pstmt.setInt(1, item.getId());
					ResultSet rs = pstmt.executeQuery();
					while (rs.next()) {
						LikeBooks likeBooks = new LikeBooks();
						likeBooks.setImg(rs.getString(1));
						likeBooks.setId(rs.getInt(2));
						likeBooks.setTitle(rs.getString(3));
						likeBooks.setAuthor(rs.getString(4));
						likeBooks.setPrice(rs.getFloat(5));
						listOfLikebooks.add(likeBooks);
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Get all like books Method Exception " + e);
		}

		return listOfLikebooks;
	}

}
