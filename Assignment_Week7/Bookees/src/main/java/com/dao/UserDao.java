package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.bean.User;
import com.resource.BookeesConnection;

public class UserDao {

	public int storeUser(User user) {
		try {
			Connection con = BookeesConnection.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("insert into user(name,email,password) values(?,?,?)");
			pstmt.setString(1, user.getName());
			pstmt.setString(2, user.getEmail());
			pstmt.setString(3, user.getPassword());
			return pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("Store method Exception" + e);
			return 0;
		}
	}
	
	public User logUser(String email,String pass) {
		User user = null;
		try {
			Connection con = BookeesConnection.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from user where email=? and password=?");
			pstmt.setString(1, email);
            pstmt.setString(2, pass);
            
            ResultSet rs = pstmt.executeQuery();
            
            if(rs.next()){
                user = new User();
                user.setName(rs.getString("name"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("password"));
            }
                
		} catch (Exception e) {
			System.out.println("LogUser method Exception" + e);
			
		}
		 return user ;
	}

}
