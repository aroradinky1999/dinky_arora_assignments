package com.service;

import com.bean.User;
import com.dao.UserDao;

public class UserService {
	
	public int storeUserInfo(User user) {
				
			UserDao ud = new UserDao();
			if(ud.storeUser(user)>0) {
				return 1;
			}else {
				return 0;
			}
		}
	
	public int logUser(String email,String pass) {
		UserDao ud = new UserDao();
		if(ud.logUser(email, pass) != null) {
			return 1;
		}else {
			return 0;
		}
	}
}



