<%@page import="com.bean.User"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.bean.Books"%>
<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%User user = (User) session.getAttribute("loguser");
  if (user==null){
	  response.sendRedirect("displaybooks.jsp");
  }
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Display books after login </title>
</head>
<link href="book.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
 

<div class="header">
     <div class="container">
        <div class="navbar">
        <nav>
            <ul id="MenuItems">
                
                <li><a href="likebooks.jsp">View Liked Books</a></li>
                 <li><a href="displaybooks.jsp">Logout</a></li>
                       
        
            </ul>
        </nav>
        </div>
    </div>

  </div> 
  
  
  <h2 class=title>Welcome user,<%=user.getEmail().substring(0,5) %>:LifeAtBookees</h2>

  <%
  Object obj = session.getAttribute("obj");
  List<Books> listOfBooks =(List<Books>) obj;
  Iterator<Books> li = listOfBooks.iterator();
  while(li.hasNext()){
	  Books books = li.next();
  
  %>

    <form action="BookController">
    <div class="small-container">
   <div class="row">
        
     <img src="<%=books.getImg()%>">
        <h4><b><%=books.getTitle() %></b></h4>
        <p><%=books.getAuthor() %>
        <div class="rating">
          <i class="fa fa-star" ></i>        
            <i class="fa fa-star" ></i>
            <i class="fa fa-star" ></i>
            <i class="fa fa-star" ></i>
            <i class="fa fa-star-o" ></i>
          </div>
        <p><%=books.getPrice() %></p>
        
        <a href="LikeBookController?id=<%= books.getId() %>">Like Book</a>
        
        </div>
         </div>

<%
}
  %>
  </form>
        
              

 




</body>
</html>