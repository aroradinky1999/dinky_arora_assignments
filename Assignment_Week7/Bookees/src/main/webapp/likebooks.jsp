<%@page import="com.resource.BookeesConnection"%>
<%@page import="com.dao.LikeBooksDao"%>
<%@page import="com.bean.LikeBooks"%>
<%@page import="com.bean.User"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.bean.Books"%>
<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%User user = (User) session.getAttribute("loguser");
  if (user==null){
	  response.sendRedirect("displaybooks.jsp");
  }
  
  ArrayList<LikeBooks> like_book = (ArrayList<LikeBooks>) session.getAttribute("like-book");
  List<LikeBooks> likeProduct = null;
  if(like_book != null){
	  LikeBooksDao lbDao = new LikeBooksDao();
	  likeProduct = lbDao.getAllLikeBooks(like_book);
	  request.setAttribute("like_book", like_book);
  }
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Display Like books </title>
</head>
<link href="book.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
 

<div class="header">
     <div class="container">
        <div class="navbar">
        <nav>
            <ul id="MenuItems">
                
                <li><a href="displaybooksafterlogin.jsp">Add More Books</a></li>
                 <li><a href="displaybooks.jsp">Logout</a></li>
                       
        
            </ul>
        </nav>
        </div>
    </div>

  </div> 
  
  
  <h2 class=title>Welcome user,<%=user.getEmail().substring(0,5) %>:LifeAtBookees:Your Liked Books</h2>
  
  <%
  if(like_book != null){
	  for(LikeBooks b : likeProduct){%>
	  
	   <div class="small-container">
   <div class="row">
        
     <img src="<%=b.getImg()%>">
        <h4><b><%=b.getTitle() %></b></h4>
        <p><%=b.getAuthor() %>
        <div class="rating">
          <i class="fa fa-star" ></i>        
            <i class="fa fa-star" ></i>
            <i class="fa fa-star" ></i>
            <i class="fa fa-star" ></i>
            <i class="fa fa-star-o" ></i>
          </div>
        <p><%=b.getPrice() %></p>
         <a href="RemoveBookController?id=<%= b.getId()%>">Remove Book</a>
         </div>
         </div>
		  
	 <%  }
  }
  %>

</body>
</html>