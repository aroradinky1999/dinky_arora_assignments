
create database Bookees_dinky;

use Bookees_dinky;

 create table User
 (
 name varchar(40),
 email varchar(40) primary key,
 password varchar(40)
 );
 create table books
 (
 img varchar(150),
 id int primary key,
 title varchar(200),
 author varchar(40),
 price float
 );
 
 insert into books values('https://upload.wikimedia.org/wikipedia/en/a/a3/BriefHistoryTime.jpg',1, 'A Brief History of Time' , 'Stephen W.Hawking',1999.00);
 insert into books values('https://images-eu.ssl-images-amazon.com/images/I/61c5DaQttjL._SX198_BO1,204,203,200_QL40_FMwebp_.jpg',2,'The Science Book','Dan Green',1724.00);
 insert into books values('https://m.media-amazon.com/images/I/51zVAiLNTZL.jpg',3,'The Sleeping Beauties','Suzanne O Sullivan',1505.00);
 insert into books values('https://images-eu.ssl-images-amazon.com/images/I/413cAh3xCfL._SY264_BO1,204,203,200_QL40_FMwebp_.jpg',4,'Breath: The New Science of a Lost Art','James Nestor',1405.00);
 insert into books values('https://m.media-amazon.com/images/I/81F7f5hAXVL._AC_UY327_FMwebp_QL65_.jpg',5,'Surely youre Joking Mr Feynman','Richard P Feynman',3004.00);
 insert into books values('https://m.media-amazon.com/images/I/61VsXzoAfDL._AC_UY327_FMwebp_QL65_.jpg',6,'What If?','Randall Munrof',3040.00);
 insert into books values('https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1617113740l/41104077.jpg',7,'Invisible Women','Caroline Criado Perez',1090.00);
 insert into books values('https://m.media-amazon.com/images/I/A16-H9wQ11L._AC_UY327_FMwebp_QL65_.jpg',8,'Under a White Sky: The Nature of the Future','Elizabeth Kolbert',2340.00);
 
  select * from books;
  
  create table likeBooks(email varchar(40) ,id int ,img varchar(150),title varchar(200),author varchar(40),foreign key (email) references  user(email),foreign key (id) references books(id));