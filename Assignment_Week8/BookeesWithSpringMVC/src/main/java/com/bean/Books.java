package com.bean;

import org.springframework.stereotype.Component;

@Component
public class Books {
	
	private String img;
	private int id;
	private String title;
	private String author;
	private float price;
	
	public Books() {
		super();
		
	}

	public Books(String img, int id, String title, String author, float price) {
		super();
		this.img = img;
		this.id = id;
		this.title = title;
		this.author = author;
		this.price = price;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Books [img=" + img + ", id=" + id + ", title=" + title + ", author=" + author + ", price=" + price
				+ "]";
	}
	
	
	
	

}
