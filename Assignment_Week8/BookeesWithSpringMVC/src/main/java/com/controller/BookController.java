package com.controller;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bean.Books;
import com.service.BookService;
import com.service.UserService;

@Controller
public class BookController {
	
	@Autowired
	BookService bookService;
	
	@Autowired
	UserService userService;
	
	
	
	@GetMapping(value = "display")
	public ModelAndView getAllBooks(HttpServletRequest req) {
		ModelAndView mav = new ModelAndView();
		Books book = new Books();
		List<Books> listOfbooks =bookService.getAllBooks();
		req.setAttribute("obj",listOfbooks);		
		mav.setViewName("displaybooks.jsp");
		return mav;
	}
	
	@GetMapping(value = "booksdisplay")
	public ModelAndView getAllBooksDisplay(HttpServletRequest req) {
		ModelAndView mav = new ModelAndView();
		Books book = new Books();
		List<Books> listOfbooks =bookService.getAllBooks();
		req.setAttribute("object",listOfbooks);		
		mav.setViewName("displaybooksafterlogin.jsp");
		return mav;
	}
	
	
	@PostMapping(value = "likebook")
	public ModelAndView storeLikeBook(HttpServletRequest request,HttpSession hs) {
		ModelAndView mav = new ModelAndView();
		
		
		int  bookId= Integer.parseInt(request.getParameter("likedBookId"));
	    String email=request.getParameter("userNumber");
		Object obj = hs.getAttribute("objBooks");
		String likedBookResult=null;
		List<Books> listOfBooks = (List<Books>)obj;
		if(listOfBooks!=null) {
			Iterator<Books> ii = listOfBooks.iterator();
			
			while(ii.hasNext()) {
				Books book = ii.next();
				
				if(bookId==book.getId()) {
					
					
					likedBookResult = userService.storeLikedBook(email, book);
					request.setAttribute("objStoreBookResult", likedBookResult);
				}
			}
		}
		hs.setAttribute("objStoreBookResult", likedBookResult);
		mav.setViewName("index.jsp");
		return mav;
	}

	
	@GetMapping(value = "likes")
	public ModelAndView likePage(HttpServletRequest request,HttpSession session) {
		ModelAndView mav = new ModelAndView();
		int  id= Integer.parseInt(request.getParameter("likedBookId"));
		String email = (String) session.getAttribute("objNumber");
		List<Books> listOfLikedBooks= bookService.getLikedBooks(email,id);
		session.setAttribute("likebooklist", listOfLikedBooks);
		mav.setViewName("likebooks.jsp");
		return mav;
	}

	
	

}
