package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.bean.LikeBooks;

@Controller
public class LikeController {

	@GetMapping(value="like")
	public ModelAndView likeButton(HttpServletRequest request,HttpServletResponse response) throws IOException {
		ModelAndView mav = new ModelAndView();
		try (PrintWriter out = response.getWriter()) {
			ArrayList<LikeBooks> likeBook = new ArrayList<>();

			int id = Integer.parseInt(request.getParameter("id"));
			LikeBooks lb = new LikeBooks();
			lb.setId(id);

			HttpSession session = request.getSession();
			ArrayList<LikeBooks> like_book = (ArrayList<LikeBooks>) session.getAttribute("like-book");

			if (like_book == null) {
				likeBook.add(lb);
				session.setAttribute("like-book", likeBook);
				mav.setViewName("displaybooksafterlogin.jsp");
				return mav;
			} else {
				likeBook = like_book;
				boolean exist = false;
				for (LikeBooks l : like_book) {
					if (l.getId() == id) {
						exist = true;
						//mav.setViewName("likebooks.jsp");
						out.print("<h3 style='color:crimson; text-align:center'>Item already exist in Liked Page.<a href = likebooks.jsp>Go to Liked Books Page</a></h3>'");
					}
				}
				if (!exist) {
					likeBook.add(lb);
					mav.setViewName("displaybooksafterlogin.jsp");
					return mav;
				}
			}

			}
			return mav;
	}
	
	
}


