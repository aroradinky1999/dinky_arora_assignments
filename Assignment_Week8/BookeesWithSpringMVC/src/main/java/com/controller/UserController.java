package com.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.bean.User;
import com.service.UserService;

@Controller
public class UserController {
	
	@Autowired
	UserService userService;
	
	@PostMapping(value = "storeuser")
	public ModelAndView storeUser(HttpServletRequest req) {
		ModelAndView mav = new ModelAndView();
		String name = req.getParameter("name");
		String email = req.getParameter("email");
		String password = req.getParameter("password");
		
		User user = new User(name, email, password);
		
		int res =userService.storeUserInfo(user);
		if(res==1) {
			mav.setViewName("login.jsp");
		}else {
			mav.setViewName("registration.jsp");
		}
		return mav;
	}
	
	@PostMapping(value = "loguser")
	public ModelAndView logUser(HttpServletRequest req) {
		ModelAndView mav = new ModelAndView();
		String email = req.getParameter("email");
		String password = req.getParameter("password");
		
		User user = new User(email, password);
		
		int res = userService.logUser(email, password);
		if(res==1) {
		req.setAttribute("logusers", user);
		mav.setViewName("userdashboard.jsp");
		}else {
			mav.setViewName("displaybooks.jsp");
		}
		return mav;
	}
	
	

		
	
	
		
}
