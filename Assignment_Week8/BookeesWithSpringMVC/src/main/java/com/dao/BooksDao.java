package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.bean.Books;
@Repository
public class BooksDao {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	

	
	public List<Books> getAllBooks() {
		try {

			return jdbcTemplate.query("select * from books",(rs,rowNum)->{
				Books books = new Books();
				books.setImg(rs.getString(1));
				books.setId(rs.getInt(2));
				books.setTitle(rs.getString(3));
				books.setAuthor(rs.getString(4));
				books.setPrice(rs.getFloat(5));
				return books;
			});
		} catch (Exception e) {
			System.out.println("In Get all books Method Exception  " + e);
		}
		return null;
	}

}
