package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.bean.Books;
import com.bean.LikeBooks;
@Repository
public class LikeBooksDao {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public List<LikeBooks> getAllLikeBooks(ArrayList<LikeBooks> likeBook) {
		List<LikeBooks> listOfLikebooks = new ArrayList<LikeBooks>();
		try {
			if (likeBook.size() > 0) {
				for (LikeBooks item : likeBook) {
			return jdbcTemplate.query("select * from books where id=?",new Object[] {item.getId()},(rs,rowNum)->{
				LikeBooks books = new LikeBooks();
				books.setImg(rs.getString("img"));
				books.setId(rs.getInt("id"));
				books.setTitle(rs.getString("title"));
				books.setAuthor(rs.getString("author"));
				books.setPrice(rs.getFloat("price"));
				  return books ;
				   
			});
				}
			}
		} catch (Exception e) {
			System.out.println("In Get all like books Method Exception  " + e);
		}
		return null ;
	}
	
	public List<Books> getLikedBooks(String email) {
		String sql="select img,id, title, author from likebooks where email="+email;
		return jdbcTemplate.query(sql,new likeBookRowMapper());
		
	}

	
	


	
	class likeBookRowMapper implements RowMapper<Books>{

		@Override
		public Books mapRow(ResultSet rs, int rowNum) throws SQLException {
			Books lb=new Books();
			lb.setImg(rs.getString(1));
			lb.setId(rs.getInt(2));
			lb.setTitle(rs.getString(3));
			lb.setAuthor(rs.getString(4));
			lb.setPrice(rs.getFloat(5));
			return lb;
		}
		
	}




}
