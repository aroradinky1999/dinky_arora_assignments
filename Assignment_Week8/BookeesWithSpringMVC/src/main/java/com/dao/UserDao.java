package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.bean.Books;
import com.bean.User;

@Repository
public class UserDao {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	
	public int storeUser(User user) {
		try {
			return jdbcTemplate.update("insert into user(name,email,password) values(?,?,?)",user.getName(),user.getEmail(), user.getPassword());	
			
		} catch (Exception e) {
			System.out.println("In storeUser method " + e);
			return 0;
		}
	}
	
	
	
	public List<User> logUser(String email,String pass) {
		try {

			return jdbcTemplate.query("select * from user where email=? and password=?",new Object[] {email,pass},(rs,rowNum)->{
				   User user = new User();
	                user.setName(rs.getString("name"));
	                user.setEmail(rs.getString("email"));
	                user.setPassword(rs.getString("password"));
				return user;
			});
		} catch (Exception e) {
			System.out.println("In LogUser method Exception " + e);
		}
		return null;
	}
	
	public int storeLikedBook(String email,Books book) {
		try {
			return jdbcTemplate.update("insert into likedBooks value(?,?,?,?,?,?)", email,
					book.getId(),book.getImg(),book.getTitle(),book.getAuthor(),book.getPrice());
			
		}catch (Exception e) {
			System.out.println("storeLikedBook method "+e);
		}
		return 0;
	}


}



