package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Books;
import com.dao.BooksDao;
import com.dao.LikeBooksDao;

@Service
public class BookService {
	
	@Autowired
	BooksDao booksDao;
	
	@Autowired
	LikeBooksDao likeBooksDao;
	
	public List<Books> getAllBooks() {
		return booksDao.getAllBooks();
		
	}
	
	public List<Books> getLikedBooks(String email,int id){
		return likeBooksDao.getLikedBooks(email); 
	}

	


	
	
	
	
	

}
