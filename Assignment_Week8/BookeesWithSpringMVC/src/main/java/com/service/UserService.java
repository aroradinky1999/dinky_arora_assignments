package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Books;
import com.bean.User;
import com.dao.UserDao;

@Service
public class UserService {
	
	@Autowired
	UserDao userDao;
	
	public int storeUserInfo(User user) {
		
		if(userDao.storeUser(user)>0) {
			return 1;
		}else {
			return 0;
		}
	}

public int logUser(String email,String pass) {
	if(userDao.logUser(email, pass) != null) {
		return 1;
	}else {
		return 0;
	}
}

public String storeLikedBook(String email, Books book) {
	if(userDao.storeLikedBook(email, book)>0) {
		return "The Book "+book.getId()+"  Added To Liked List";
		
	}else {  
		return "Failed To Add The Book "+book.getId()+" To Liked List";
	}
}

}



