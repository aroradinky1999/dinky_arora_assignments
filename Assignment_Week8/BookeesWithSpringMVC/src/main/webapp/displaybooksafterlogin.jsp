<%@page import="com.bean.User"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.bean.Books"%>
<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Display books after login </title>
</head>
<link href="book.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<div class="header">
     <div class="container">
        <div class="navbar">
        <nav>
            <ul id="MenuItems">
                
                <li><a href="likebooks.jsp">View Liked Books</a></li>
                <li><a href="like.spring">View Read Later Books</a></li>
                 <li><a href="display.spring">Logout</a></li>
                       
        
            </ul>
        </nav>
        </div>
    </div>

  </div> 
  
  
  <%
  Object obj = request.getAttribute("object");
  List<Books> listOfBooks =(List<Books>) obj;
  Iterator<Books> li = listOfBooks.iterator();
  while(li.hasNext()){
	  Books books = li.next();
  
  %>

    <form action="booksdisplay.spring" method="get">
    <div class="small-container">
   <div class="row">
        
     <img src="<%=books.getImg()%>">
        <h4><b><%=books.getTitle() %></b></h4>
        <p><%=books.getAuthor() %>
        <div class="rating">
          <i class="fa fa-star" ></i>        
            <i class="fa fa-star" ></i>
            <i class="fa fa-star" ></i>
            <i class="fa fa-star" ></i>
            <i class="fa fa-star-o" ></i>
          </div>
        <p><%=books.getPrice() %></p>
        
        
       <form action="likebook.spring" method="post">
		<input type="hidden" name="id" value=<%=books.getId()%>>
		<input type="submit" value="Like" name="Like">
		</form>
        
        <form action="like.spring">
		<input type="hidden" name="id" value=<%=books.getId()%>>
		<input type="submit" value="Read Later">
		</form>
		
        </div>
         </div>

<%
}
  %>
  </form>
 
        

</body>
</html>