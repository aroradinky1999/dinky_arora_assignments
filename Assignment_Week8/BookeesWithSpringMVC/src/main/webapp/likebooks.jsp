
<%@page import="com.dao.LikeBooksDao"%>
<%@page import="com.bean.LikeBooks"%>
<%@page import="com.bean.User"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.bean.Books"%>
<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%
		
		Object objNumber = session.getAttribute("objNumber");
%>

	
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Display Like books </title>
</head>
<link href="book.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
 

<div class="header">
     <div class="container">
        <div class="navbar">
        <nav>
            <ul id="MenuItems">
                
                <li><a href="booksdisplay.spring">Add More Books</a></li>
                 <li><a href="display.spring">Logout</a></li>
                       
        
            </ul>
        </nav>
        </div>
    </div>

  </div> 
  
  
  <h2 class=title>Welcome:LifeAtBookees:Your Liked Books</h2>
  
 <% 
 Object objLikedBook= session.getAttribute("likebooklist");
 List<Books> listOfLikedBooks = (List<Books>)objLikedBook;
 Iterator<Books> ii = listOfLikedBooks.iterator();
 			while(ii.hasNext()){
 				Books book = ii.next();
 				
 				
 				
 					%>

 
 
 
 
 
	  <form action="like.spring" method="get">
	   <div class="small-container">
   <div class="row">
        
     <img src="<%=book.getImg()%>">
        <h4><b><%=book.getTitle() %></b></h4>
        <p><%=book.getAuthor() %>
        <div class="rating">
          <i class="fa fa-star" ></i>        
            <i class="fa fa-star" ></i>
            <i class="fa fa-star" ></i>
            <i class="fa fa-star" ></i>
            <i class="fa fa-star-o" ></i>
          </div>
        <p><%=book.getPrice() %></p>
       
         </div>
         </div>
		  
	 <% 
	 }
  
	 
  
  %>
</form>




</body>
</html>