<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login Page</title>
	<link href="https://fonts.googleapis.com/css?family=ZCOOL+XiaoWei"
		rel="stylesheet">
	<link href="loginRegister.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="container">
		<div class="box">
			<img class="img" src="https://cdn3.iconfinder.com/data/icons/login-4/512/LOGIN-10-512.png">
			<h1>Login Account</h1>
			<form action="loguser.spring" method="post">
				<p>Username</p>
				<input type="text" placeholder="Username" name="email" required>
				<p>Password</p>
				<input type="password" placeholder="Password" name="password"
					required> <input type="submit" value="Login"> <br>
					<a href="registration.jsp">Create New Account</a>
			</form>
		</div>
	</div>
</body>

</html>