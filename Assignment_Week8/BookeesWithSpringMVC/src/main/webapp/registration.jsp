<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Register Account</title>
	<link href="https://fonts.googleapis.com/css?family=ZCOOL+XiaoWei"
		rel="stylesheet">
	<link href="loginRegister.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="container">
		<div class="regbox box">
			<img class="img" src="https://www.pikpng.com/pngl/m/29-292243_people-hand-drawn-persons-group-svg-png-icon.png">
			<h1>Register Account</h1>
			<form action="storeuser.spring" method="post">
				<p>Username</p>
				<input type="text" placeholder="Username" name="name" required>
				<p>Useremail</p>
				<input type="text" placeholder="Useremail" name="email" required>
				<p>Password</p>
				<input type="password" placeholder="Password" name="password"
					required> <input type="submit" value="Register"> <a
					href="login.jsp">Already have Account?</a>
			</form>
		</div>
	</div>
</body>

</html>