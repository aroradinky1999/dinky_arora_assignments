package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.AdminDao;
import com.entity.Admin;

@Service
public class AdminService {

	@Autowired
	AdminDao adminDao;

	public String registerAdminInfo(Admin admin) { 
			adminDao.save(admin);
			return "Admin Details stored successfully";
			
		}
	
	
	public String login(Admin admin) {
		if(!adminDao.existsById(admin.getEmail())) {
			return "Either email or password is incorrect";
		}
		else {
			Admin a = adminDao.getById(admin.getEmail());
			if (a.getPassword().equals(admin.getPassword())) {
				return "Sucessfully logged in";
			}
			else {
				return "Failed to login";
			}
		}
	}

	public String logOut(String email ) {
		if(adminDao.existsById(email)) {
			return "Successfully Log Out";
		}
		else {
			return "Failed to Log Out";
		}
		
	}

	
	
	

}

	
