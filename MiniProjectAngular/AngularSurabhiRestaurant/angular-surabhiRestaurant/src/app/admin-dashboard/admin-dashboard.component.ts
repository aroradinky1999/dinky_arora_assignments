import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
name:string="";
flag:boolean=false;
addUserRes:string="";
delUserRes:string="";
updateUserRes:string="";
users:Array<User>=[];


AddUserRef = new FormGroup({
  email:new FormControl("", Validators.required),
  name:new FormControl("",Validators.required),
  password:new FormControl("", Validators.required)
});
UpdateUserRef = new FormGroup({
  email:new FormControl("", Validators.required),
  name:new FormControl("",Validators.required),
  password:new FormControl("", Validators.required)
});
  constructor(public router:Router,public user:UserService) {
   
   }

  ngOnInit(): void {
    this.loadUsers();

    let obj = sessionStorage.getItem("uname");
    if(obj != null ){
      this.name =obj;
    }
  }

  logout(){
 this.router.navigate(["adminLogin"]);
  }
 

  // crud for user

  storeUser():void{
    let adduser = this.AddUserRef.value; 
    this.user.addUserDetails(adduser).subscribe(res=>this.addUserRes=res,error=>console.log(error),()=>this.loadUsers());
    this.AddUserRef.reset();
  }
  loadUsers():void{
    this.user.loadUserDetails().subscribe(res=>this.users=res)
  }
  deleteUser(email:string){
    this.user.deleteUserDetails(email).subscribe(res=>this.delUserRes=res,error=>console.log(error),()=>this.loadUsers());
  }
  updateUser(userss:User){
  this.flag=true; 
  }

  updateUserInfo():void{
    let updateuser = this.UpdateUserRef.value;
  this.user.updateUserDetails(updateuser).subscribe(res=>this.updateUserRes=res,error=>console.log(error),()=>this.loadUsers());
  this.UpdateUserRef.reset();
  }
}
