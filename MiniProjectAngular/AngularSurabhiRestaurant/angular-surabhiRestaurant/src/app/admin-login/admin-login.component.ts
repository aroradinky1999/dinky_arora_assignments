import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {
 loginRef = new FormGroup({
   email:new FormControl("", Validators.required),
   password:new FormControl("", Validators.required)
 });
 RegisterRef = new FormGroup({
  email:new FormControl("", Validators.required),
  password:new FormControl("", Validators.required)
});
loginRes:string="";
registerRes:string="";

  constructor(public admin:AdminService,public route:Router) { }

  ngOnInit(): void {
  }
  checkAdmin():void{
    let login = this.loginRef.value;
    this.loginRes="";
    //console.log(login); 
    this.admin.adminLoginDetails(login).subscribe(res=>{
      if(res.includes('Sucessfully logged in')){
        sessionStorage.setItem("uname",login.email);
        this.route.navigate(['adminHome']);
   }else{
    this.loginRes = 'Failed to login!!Please enter correct email and password'
   }

    },error=>console.log(error));
    
  }

  RegisterAdmin():void{
    let register = this.RegisterRef.value;
    this.registerRes="";
   // console.log(register); 
   this.admin.adminRegisterDetails(register).subscribe(res=>this.registerRes=res);
   this.RegisterRef.reset();
     
  }
    


  

}
