import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http';
import { Admin } from './admin';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(
    public http:HttpClient
  ) { }

  adminLoginDetails(admin:Admin):Observable<string>{
    return this.http.post("http://localhost:8585/admin/adminLogin",admin,{responseType:'text'})

  }

  adminRegisterDetails(admin:Admin):Observable<string>{
    return this.http.post("http://localhost:8585/admin/adminRegister",admin,{responseType:'text'})
    }
  

}
