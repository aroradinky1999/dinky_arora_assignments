import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { Items } from './items';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  constructor(public http:HttpClient) { }

  loadItemDetails():Observable<Items[]>{
  return this.http.get<Items[]>("http://localhost:8686/items/getAllItems")
  }
  addItemDetails(book:Items):Observable<string>{
    return this.http.post("http://localhost:8686/items/storeItems",book,{responseType:'text'})
    }

  deleteItemDetails(id:number):Observable<string>{
      return this.http.delete("http://localhost:8686/items/deleteItems/"+id,{responseType:'text'})
    }

  updateItemDetails(book:Items):Observable<string>{
        return this.http.put("http://localhost:8686/items/updateItems",book,{responseType:'text'})
    }

}
