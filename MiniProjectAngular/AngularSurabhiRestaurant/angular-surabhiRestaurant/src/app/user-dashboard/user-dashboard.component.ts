import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Items } from '../items';
import { ItemsService } from '../items.service';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit {
name:string='';
items:Array<Items>=[];
  constructor(public router:Router,public itemService:ItemsService) { 
    
  }

  ngOnInit(): void {
    let obj = sessionStorage.getItem("uname");
    if(obj != null ){
      this.name =obj;
    }
  }
  logout(){
    this.router.navigate(["userLogin"]);
     }
     loadProducts():void{
      this.itemService.loadItemDetails().subscribe(res=>this.items=res)
    }

}
