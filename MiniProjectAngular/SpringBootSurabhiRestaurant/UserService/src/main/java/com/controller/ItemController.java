package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.entity.Items;

import com.service.ItemsService;

@RestController
@RequestMapping("/items")
@CrossOrigin
public class ItemController {
	
	@Autowired
	ItemsService itemService;

	@GetMapping(value = "getAllItems", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Items> getAllBookssInfo() {
		return itemService.getAllItems();
	}

	@PostMapping(value = "storeItems", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeBooksInfo(@RequestBody Items item) {
		return itemService.addItems(item);
	}

	@DeleteMapping(value = "deleteItems/{id}")
	public String deleteBooksInfo(@PathVariable("id") int id) {
		return itemService.deleteItem(id);
	}
	
	@PutMapping(value = "updateItems")
	public String updateBooksInfo(@RequestBody Items item) {
		return itemService.updateItem(item);
	}
	
}


