package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.User;
import com.service.UserService;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {
	
	@Autowired
	UserService userService;
	
	@PostMapping(value = "userLogin", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String userLogin( @RequestBody User user) {
		return userService.login(user);
	}

	@PostMapping(value = "userRegister", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String userRegister(@RequestBody User user) {
		return userService.storeUserInfo(user);
	}
	
	@GetMapping(value = "userLogOut/{email}")
	public String logout(@PathVariable("email") String email) {
		return userService.logOut(email);
	}
	
	@GetMapping(value="getuser",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<User> getAllUser(){
		return userService.getAllUserDetails();
	}
	
	@PostMapping(value = "storeUser", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeUserInfo(@RequestBody User user) {
		return userService.storeUserInfo(user);
	}

	@DeleteMapping(value = "deleteUser/{email}")
	public String deleteUserInfo(@PathVariable("email") String email) {
		return userService.deleteUserInfo(email);
	}
	
	@PutMapping(value = "updateUser")
	public String updateUserPass(@RequestBody User user) {
		return userService.updateUser(user);
	}
	
	

	
	



}
