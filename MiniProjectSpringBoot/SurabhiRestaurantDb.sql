
create database Miniproject_dinky;

use Miniproject_dinky;

create table Admin
 (
 email varchar(40) primary key,
 password varchar(40)
 );
 insert into Admin values('admin@gmail.com','admin');
 
 create table Users
 (
 name varchar(50),
 email varchar(50) primary key ,
 password varchar(100)
 );
 
  
 create table items
 (
 id int primary key,
 name varchar(200),
 price float
 );
 
 insert into items values(1, 'Momos' ,110.00);
 insert into items values(2,'Fried Rice',172.00);
 insert into items values(3,'Manchurian',150.00);
 insert into items values(4,'Spring rool',140.00);
 insert into items values(5,'Pizza',300.00);
 insert into items values(6,'Burgers',90.00);
 insert into items values(7,'Coldrink',110.00);
 insert into items values(8,'Smoothie',230.00);
 
  select * from items;
  
  
  
CREATE TABLE mycart(email varchar(100),item_id int,item_name varchar(100),quantity int,total_price float,foreign key (email) references users(email),foreign key (item_id) references items(id),primary key (email,item_id));

 