package com.bean;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity

@Table(name = "mycart")
public class MyCart {
	@EmbeddedId
	private Cart cart;
	private String itemName;
	private int quantity;
	private float totalPrice;
	public MyCart() {
		super();
		
	}
	public MyCart(Cart cart, String itemName, int quantity, float totalPrice) {
		super();
		this.cart = cart;
		this.itemName = itemName;
		this.quantity = quantity;
		this.totalPrice = totalPrice;
	}
	public Cart getCart() {
		return cart;
	}
	public void setCart(Cart cart) {
		this.cart = cart;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public float getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(float totalPrice) {
		this.totalPrice = totalPrice;
	}
	@Override
	public String toString() {
		return "MyCart [cart=" + cart + ", itemName=" + itemName + ", quantity=" + quantity + ", totalPrice="
				+ totalPrice + "]";
	}
	
	

}
