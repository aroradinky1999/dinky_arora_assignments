package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bean.Admin;

import com.service.AdminService;


@Controller
@RequestMapping("/admin")
public class AdminController {
	
	@Autowired
	AdminService adminService;
	
	
	@GetMapping(value = "openAdminLogin")
	public String openadminLogin() {
		return "adminLogin";
	}
	
	@GetMapping(value = "adminHome")
	public String openAdminHome() {
		return "adminHome";
	}
	
	@GetMapping(value = "adminLogout")
	public String adminLogout(HttpSession session) {
		session.invalidate();
		return "index";

	}

	
	@PostMapping(value = "adminLogin")
	public String loginadmin(HttpServletRequest request, HttpSession hs) {

		String email = request.getParameter("email");
		String password = request.getParameter("password");

		Admin admin = new Admin(email, password);

		try {
		String res = adminService.logAdminInfo(admin);
		if (res.contains("Sucessfully")) {
			hs.setAttribute("logadmin", admin);
			return "adminHome";
		} else if (res.contains("Failed")) {
			return "adminLogin";
		}
		return "index";
	}catch(Exception e) {
		System.out.println("Con Errorr"+e);
	}
		return null;
}
}
	
	
	
	


