package com.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bean.Items;
import com.bean.Users;
import com.service.ItemsService;

@Controller
@RequestMapping("/items")
public class ItemsController {
	
	@Autowired
	ItemsService itemsService;
	
	@GetMapping(value = "getAllItems")
	public String getAllItems(HttpServletRequest request, HttpSession session) {
		Items items = new Items();
		List<Items> listOfItems = itemsService.getAllItems();
		session.setAttribute("item", listOfItems);
		return "menuItems";
	}
	
	@GetMapping(value = "getAllItem")
	public String getAllItem(HttpServletRequest request, HttpSession session) {
		Items items = new Items();
		List<Items> listOfItems = itemsService.getAllItems();
		session.setAttribute("itemlist", listOfItems);
		return "displayItems";
	}
	
	
	

	@GetMapping(value = "openaddItem")
	public String openAddMenuPage() {
		return "addItem";
	}

	
	@PostMapping(value = "addItem")
	public String addItem(HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		float price = Float.parseFloat(request.getParameter("price"));
		String addMenuResult = itemsService.addItems(new Items(id, name, price));
		return "addItem";
	}

	
	@GetMapping(value = "openupdateItem")
	public String openUpdateItem() {
		return "updateItem";
	}

	
	@PostMapping(value = "updateItem")
	public String updateItemName(HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		float price = Float.parseFloat(request.getParameter("price"));
		 itemsService.updateItem(new Items(id,name, price));
		return "updateItem";
	}

	@GetMapping(value = "opendeleteItem")
	public String openDeleteItemPage() {
		return "deleteItem";
	}

	
	@PostMapping(value = "deleteItem")
	public String deleteItem(HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
	    itemsService.deleteItem(id);		
		return "deleteItem";
	}



}
