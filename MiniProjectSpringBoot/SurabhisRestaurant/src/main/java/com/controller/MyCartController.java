package com.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bean.Cart;
import com.bean.MyCart;
import com.service.MyCartService;


@Controller
@RequestMapping("/cart")
public class MyCartController {
	
	@Autowired
	MyCartService myCartService;
	
	@PostMapping(value = "cartService")
	public String order(HttpServletRequest request, HttpSession hs) {
		String select = request.getParameter("items");
		String email = (String) hs.getAttribute("objEmail");
		String orderResult;
		if (select != null) {

			String getID = request.getParameter("id");
			String name = request.getParameter("name");
			float price = Float.parseFloat(request.getParameter("price"));
			String q = request.getParameter("quantity");
			int quantity = Integer.parseInt(q);
			int id = Integer.parseInt(getID);

			MyCart cart = new MyCart();
			cart.setCart(new Cart(email, id));
			cart.setItemName(name);
			cart.setQuantity(quantity);
			cart.setTotalPrice(price * quantity);

			orderResult = myCartService.addToCart(cart);
			request.setAttribute("objSelect", orderResult);

		} else {
			orderResult = "Please Select The Item";
			request.setAttribute("objSelect", orderResult);
		}
		return "menuItems";

	}

	
	@GetMapping(value = "addToCart")
	public String userCart(HttpServletRequest request, HttpSession hs) {
		String email = (String) hs.getAttribute("objEmail");

		List<MyCart> listOfCart = myCartService.getCart(email);

		request.setAttribute("objCart", listOfCart);
		if (!listOfCart.isEmpty()) {
			float total = myCartService.getTotal(email);
			request.setAttribute("objTotal", total);
		}
		return "addToCart";
	}
	
	
	
	
	
	

}
