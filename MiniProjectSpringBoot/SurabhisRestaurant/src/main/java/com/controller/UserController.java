package com.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bean.Users;
import com.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserService userService;
	
	@GetMapping(value = "openUserLogin")
	public String openUserLogin() {
		return "userLogin";
	}

	@GetMapping(value = "openUserRegister")
	public String openUserRegister() {
		return "userRegister";
	}
	
	@GetMapping(value = "openUserHome")
	public String openUserHome() {
		return "userHome";
	}
	
	@GetMapping(value = "userLogout")
	public String userLogout(HttpSession session) {
		session.invalidate();
		return "index";
	}

	
	@PostMapping(value = "userRegister")
	public String registerUser(HttpServletRequest req, HttpSession hs) {
		String name = req.getParameter("name");
		String email = req.getParameter("email");
		String password = req.getParameter("password");
		Users user = new Users(name, email, password);
		String res = userService.storeUserInfo(user);
		if (res.contains("Successfully")) {
	        hs.setAttribute("objEmail", email);
			return "userLogin";
		} else if (res.contains("Failed")) {
			return "userRegister";
		}
		return "index";

	}

	@PostMapping(value = "userLogin")
	public String loginUser(HttpServletRequest request, HttpSession session) {

		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String name = email.substring(0, email.indexOf('@'));
		Users user = new Users(email, password);
		String res = userService.logUserInfo(user);
		if (res.contains("Successfully")) {
			session.setAttribute("objEmail", email);
			session.setAttribute("name", name.toUpperCase());
			session.setAttribute("loguser", user);
			return "userHome";
		} else if (res.contains("Failed")) {
			return "userLogin";
		}
		return "index";
	}
	
	@GetMapping(value = "getAllUsers")
	public String getAllUsers(HttpServletRequest request) {
		List<Users> listOfUsers = userService.getAllUsers();
		request.setAttribute("user", listOfUsers);
		return "displayUser";
	}

	@GetMapping(value = "openAddUser")
	public String openAddUser() {
		return "addUser";
	}

	@PostMapping(value = "addUser")
	public String addNewUser(HttpServletRequest request) {
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		 userService.storeUserInfo(new Users(name,email, password));
		return "addUser";
	}
	
	

	@GetMapping(value = "openUpdateUser")
	public String openUpdateUser() {
		return "updateUser";
	}

	@PostMapping(value = "update")
	public String updatePassword(HttpServletRequest request) {
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
	     userService.updateUserInfo(new Users(name,email, password));
		return "updateUser";
	}

	@GetMapping(value = "openDeleteUser")
	public String openDeleteUser() {
		return "deleteUser";
	}

	@PostMapping(value = "deleteUser")
	public String deleteUser(HttpServletRequest request) {
		String email = request.getParameter("email");
		 userService.deleteUserInfo(email);
		
		return "deleteUser";
	}


}
