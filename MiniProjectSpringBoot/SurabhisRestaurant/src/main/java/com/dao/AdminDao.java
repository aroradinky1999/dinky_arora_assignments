package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bean.Admin;


@Repository
public interface AdminDao extends JpaRepository<Admin, String>{
	
	//public Admin findByEmailAndPassword(@Param("email") String email,@Param("pass") String password);

}
