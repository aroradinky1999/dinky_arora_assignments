package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bean.Items;

@Repository
public interface ItemsDao extends JpaRepository<Items, Integer>{
		

}
