package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bean.Cart;
import com.bean.MyCart;


@Repository
public interface MyCartDao extends JpaRepository<MyCart, Cart> {
	
	@Query("select i from MyCart i where i.cart.email=:email")
	public List<MyCart> getMyCart(@Param("email") String email);

	

	@Query("select sum(i.totalPrice) from MyCart i where i.cart.email=:email")
	public float getTotal(@Param("email") String email);

}
