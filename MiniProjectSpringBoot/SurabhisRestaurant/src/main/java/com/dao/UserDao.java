package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bean.Users;

@Repository
public interface UserDao extends JpaRepository<Users, String> {
     @Query("select u from Users u where u.email=:user and u.password = :pass")
	public Users loginUser(@Param("user") String email,@Param("pass") String password);

}
