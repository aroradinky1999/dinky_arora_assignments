package com.service;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Admin;
import com.dao.AdminDao;


@Service
public class AdminService {

	@Autowired
	AdminDao adminDao;
	
	public String logAdminInfo(Admin admin) {
		try {
		if(adminDao.existsById(admin.getEmail())) {
			Admin a = adminDao.getById(admin.getEmail());
			if (a.getPassword().equals(admin.getPassword())) {
				return "Sucessfully logged in";
			}
			else {
				return "Failed to login";
			}
	
		}
		else {
			return "Failed!!Either email or password is incorrect";
		}
	}catch(Exception e) {
		System.out.println("Errrroorrrrrrr"+e);
	}
	return null;
}
}
