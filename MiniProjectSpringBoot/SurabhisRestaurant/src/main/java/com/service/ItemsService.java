package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Items;
import com.dao.ItemsDao;

@Service
public class ItemsService {
	@Autowired
	ItemsDao itemsDao;
	
	public List<Items> getAllItems() {
		return itemsDao.findAll();
		
	}
	
	public String addItems(Items item) {
		if (itemsDao.existsById(item.getId())) {
			return "Item ID must be unique ";
		} else {
			itemsDao.save(item);
			return "Item Stored Successfully";
		}

	}
	
	public String updateItem( Items item) {
		if (!itemsDao.existsById(item.getId())) {
			return "Failed to Update";
		} else {
			Items i = itemsDao.getById(item.getId());
			i.setName(item.getName());
			i.setPrice(item.getPrice());
			itemsDao.saveAndFlush(i);
			return "Item  Updated Successfully";
		}
	}
	
	public String deleteItem(int id) {
		if (!itemsDao.existsById(id)) {
			return "Failed to Update";
		} else {
			itemsDao.deleteById(id);
			return "Item Deleted Successfully ";
		}

	}





}
