package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.MyCart;
import com.bean.Users;
import com.dao.MyCartDao;

@Service
public class MyCartService {
	@Autowired
	MyCartDao myCartDao;
	
	public String addToCart(MyCart cart) {
		if (myCartDao.existsById(cart.getCart())) {
			return "Item already present in the cart";
		} else {
			myCartDao.save(cart);
			return "Item  added to the cart";
		}

	}

	
	public List<MyCart> getCart(String email) {

		return myCartDao.getMyCart(email);
	}
	
	public float getTotal(String email) {
		return myCartDao.getTotal(email);
	}



	
}
