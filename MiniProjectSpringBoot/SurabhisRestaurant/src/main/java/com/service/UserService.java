package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Users;
import com.dao.UserDao;

@Service
public class UserService {

	@Autowired
	UserDao userDao;

	public String storeUserInfo(Users user) {
		if (userDao.existsById(user.getEmail())) {
			return "Failed to store user details";
		} else {
			userDao.save(user);
			return "User Stored Successfully";
		}

	}

	

	public String logUserInfo(Users user) {
	if(!userDao.existsById(user.getEmail())) {
		return "Either email or password is incorrect";
	}
	else {
		Users u = userDao.loginUser(user.getEmail(), user.getPassword());
		if (u.getPassword().equals(user.getPassword())) {
			return "Successfully logged in";
		}
		else {
			return "Failed to login";
		}
	}
	}
	
	public List<Users> getAllUsers() {
		List<Users> listOfUsers = userDao.findAll();
		return listOfUsers;
	}

	
	public String updateUserInfo(Users user) {
		if (!userDao.existsById(user.getEmail())) {
			return "User details not available";
		} else {
			Users u = userDao.getById(user.getEmail());
			u.setName(user.getName());
			u.setPassword(user.getPassword());
			userDao.saveAndFlush(u);
			return " User Updated Successfully";
		}
	}

	
	public String deleteUserInfo(String email) {
		if (!userDao.existsById(email)) {
			return "User details not available";
		} else {
			userDao.deleteById(email);
			return "User Deleted Successfully";
		}
	}
}



