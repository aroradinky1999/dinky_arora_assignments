<%@page import="org.hibernate.internal.build.AllowSysOut"%>
<%@page import="com.bean.Users"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.bean.Items"%>
<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin Home</title>
<link href="/css/adminHome.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<div class="header">
		<div class="container">
			<div class="navbar">
				<nav>
					<ul id="MenuItems">
			
						<li><a href="/admin/adminLogout">Logout</a></li>



					</ul>
				</nav>
			</div>
		</div>

	</div>

                 
	

	<h1 class=title>Surabhi's Restaurant</h1>
	<h4 class=title>
		Welcome,Admin
		</h4>
		
		<div class="small-container">
			<div class="row">				
				<h3>Crud On Users</h3>
				
			</div>
		</div>
		
		<a href= "/user/openAddUser" class="btn">Add User &#8594;</a>
         <a href= "/user/getAllUsers" class="btn">Display User  &#8594;</a>
         <a href= "/user/openUpdateUser" class="btn">Update User  &#8594;</a>
         <a href= "/user/openDeleteUser" class="btn">Delete User  &#8594;</a>
	     
	     
	     <div class="small-container">
			<div class="row">				
				<h3>Crud On Menu Items</h3>
				
			</div>
		</div>
		
		<a href= "/items/openaddItem" class="btn">Add Item &#8594;</a>
         <a href= "/items/getAllItem" class="btn">Display Items  &#8594;</a>
         <a href= "/items/openupdateItem" class="btn">Update Item  &#8594;</a>
         <a href= "/items/opendeleteItem" class="btn">Delete Item  &#8594;</a>
	
	<div class="small-container">
			<div class="row">				
				<h3>Orders</h3>
				
			</div>
		</div>
		
		<a href= "" class="btn">Bills generated today &#8594;</a>
         <a href= "" class="btn">Total sale for this month  &#8594;</a>
         <a href= "" class="btn">Orders done by specific users  &#8594;</a>
         
	     


</body>
</html>