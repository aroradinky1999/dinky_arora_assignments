<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<title>Admin Login Page</title>
	<link href="https://fonts.googleapis.com/css?family=ZCOOL+XiaoWei"
		rel="stylesheet">
	<link href="/css/registerLogin.css" rel="stylesheet" type="text/css" />
</head>
<body>

	<div class="container">
		<div class="box">
			<img class="img" src="https://cdn3.iconfinder.com/data/icons/login-4/512/LOGIN-10-512.png">
			<h1>Login Account</h1>
			<form action="/admin/adminLogin" method="post">
				<p>Username</p>
				<input type="text" placeholder="Username" name="email" required>
				<p>Password</p>
				<input type="password" placeholder="Password" name="password"
					required> <input type="submit" value="Login">
			</form>
		</div>
	</div>
</body>


</html>