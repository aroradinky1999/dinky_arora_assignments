<%@page import="com.bean.Items"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.bean.Users"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Display Items</title>
<link href="/css/userHome.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>
<h1 class=title>All Items</h1>

		
		<div class="small-container">
			<div class="row">
			<p><b>Item Id </b></p>
			<p><b>Item Name</b></p>
			<p><b>Item Price</b></p>
			</div>
		</div>

	<%
	try {
		Object obj = session.getAttribute("itemlist");
		List<Items> listOfItems = (List<Items>) obj;
		Iterator<Items> li = listOfItems.iterator();
		while (li.hasNext()) {
			Items items = li.next();
	%>

	<form action="/item/getAllItems" method="get">
		<div class="small-container">
			<div class="row">


				
				<p><%=items.getId()%></p>				
				<p><%=items.getName()%>
				<p><%=items.getPrice()%></p>
				
				
			</div>
		</div>

		<%
		}
		} catch (Exception e) {
		out.print(e);
		}
		%>
	</form>
	
	<a href= "/admin/adminHome" class="btn">Admin Home &#8594;</a>


</body>
</html>