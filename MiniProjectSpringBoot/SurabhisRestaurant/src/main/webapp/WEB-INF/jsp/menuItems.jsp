<%@page import="org.hibernate.internal.build.AllowSysOut"%>
<%@page import="com.bean.Users"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.bean.Items"%>
<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
Object name = session.getAttribute("name");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Display Items</title>
<link href="/css/userHome.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

	<div class="header">
		<div class="container">
			<div class="navbar">
				<nav>
					<ul id="MenuItems">
                         <li><a href="/user/openUserHome">Home</a></li>
						<li><a href="/cart/addToCart">My Cart</a></li>
						<li><a href="">My Orders</a></li>
						<li><a href="/user/userLogout">Logout</a></li>
						


					</ul>
				</nav>
			</div>
		</div>

	</div>

	<h1 class=title>Surabhi's Restaurant</h1>
	<h4 class=title>
		Welcome
		<%=name%></h4>
		
		<div class="small-container">
			<div class="row">
			<p><b>Item Id</b></p>
			<p><b>Item Name</b></p>
			<p><b>Item Price</b></p>
			<p><b>Quantity</b></p>
			<p><b>Add To Cart</b></p>
			</div>
		</div>

	<%
	try {
		Object obj = session.getAttribute("item");
		List<Items> listOfItems = (List<Items>) obj;
		Iterator<Items> li = listOfItems.iterator();
		while (li.hasNext()) {
			Items items = li.next();
	%>

	<form action="/items/getAllItems" method="get">
		<div class="small-container">
			<div class="row">


				
					<p><%=items.getId()%></p>
				
				<p><%=items.getName()%>
				<p><%=items.getPrice()%></p>
				
				
				<form action="/cart/cartService" method="post">
				<input type="number" min="1" value="1"
					name="quantity">
				
					<input type="hidden" value="<%=items.getId() %>" name="id">
					<input type="hidden" value="<%=items.getName() %>" name="name">
					<input type="hidden" value="<%= items.getPrice()%>" name="price">
				
				<input  type="submit" value="Add To Cart" class= "btn" />
			</form>
				
				



			</div>
		</div>

		<%
		}
		} catch (Exception e) {
		out.print(e);
		}
		%>
	</form>







</body>
</html>