<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>Update Item</title>
	<link href="https://fonts.googleapis.com/css?family=ZCOOL+XiaoWei"
		rel="stylesheet">
	<link href="/css/registerLogin.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="container">
		<div class="regbox box">
			<img class="img" src="https://thumbs.dreamstime.com/b/junk-food-concept-unhealthy-food-background-fast-food-sugar-burger-sweets-chips-chocolate-donuts-soda-junk-food-concept-137097176.jpg">
			<h1>Update Item</h1>
			<form action="/items/updateItem" method="post">
				<p>Item Id</p>
				<input type="text" placeholder="Item Id" name="id" required>
				<p>Item Name</p>
				<input type="text" placeholder="Item Name" name="name" required>
				<p>Item Price</p>
				<input type="text" placeholder="Item Price" name="price"
					required> <input type="submit" value="Update Item"> <a
					href="/items/getAllItem">View All Items</a>
			</form>
		</div>
	</div>
</body>


</html>